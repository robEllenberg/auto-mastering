import numpy as np
import numpy.linalg as linalg

from typing import List


class JointLevelInterpolator:

    def __init__(self, joint_angles: List[float], level_readings: List[float]):

        # Express as column vectors
        x = np.mat(joint_angles).transpose()
        y = np.mat(level_readings).transpose()

        A = np.hstack((x, np.ones(x.shape)))
        b = y
        est, residuals, _, _ = linalg.lstsq(A, b)
        self.slope = est[0]
        self.intercept = est[1]
        self.residual_error = linalg.norm(residuals)

    def findzero(self):
        return float(-self.intercept / self.slope)


class JointLevelFinder:
    """
    As more level / joint data is available, update the model to predict the actual level point
    """

    def __init__(self, max_level_angle, level_precision_window, residual_tolerance, jitter, slope_guess=1.0):

        # Start with some dummy data based on our assumption of a perfectly level joint (will be pruned off later)
        self.joint_angles = []
        self.level_readings = []
        # Assume the joint is perfectly level to start
        self.slope = slope_guess
        self.intercept = 0
        # Some dummy value until we get real data
        self.residual_error = 1000
        # Defines how far the level can tilt before we don't trust the reading
        self.max_level_angle = max_level_angle
        self.level_precision_window = level_precision_window # Window defines a +/- range around zero where the level is most accurate
        self.jitter_seq = np.array([-1., 1., 0.])
        self.jitter_mag = jitter
        self.jitter_cnt = -1
        self.tolerance = residual_tolerance

    def update(self, joint_angle, level_reading):
        self.joint_angles.append(joint_angle)
        self.level_readings.append(level_reading)
        self._update_model()
        return self.findzero()

    def count_good_measurements(self):
        return np.count_nonzero(abs(np.array(self.level_readings)) <= self.max_level_angle)

    def _update_model(self):
        # Express as column vectors
        x = np.mat(self.joint_angles[-3:]).transpose()
        y = np.mat(self.level_readings[-3:]).transpose()

        # Readings closer to level are more accurate (with readings between +/-0.5 deg accurate to 0.02 deg)
        # Use inverse-square to reduce the weights of readings far from level
        reading_accuracy_heuristic = 1. / np.maximum(np.square(self.level_readings[-3:]), self.level_precision_window ** 2.)
        W_sqrt = np.mat(np.diag(np.sqrt(reading_accuracy_heuristic)))
        # Add weights to the A / b matrices using the modified weight matrix (since numpy doesn't have a native weighted least squares implementation)
        A = W_sqrt * np.hstack((x, np.ones(x.shape)))
        b = W_sqrt * y
        est, residual_norm, _, _ = linalg.lstsq(A, b)
        if len(self.joint_angles) < 2:
            # Reuse the slope guess
            self.intercept = self.level_readings[-1] - self.slope*self.joint_angles[-1]
        else:
            self.slope = est[0]
            self.intercept = est[1]
        self.residual_error = float(residual_norm or 1000)

    def findzero(self):
        return float(-self.intercept) / float(self.slope)

    def next_testpoint(self):
        jitter = self.jitter_mag
        # Otherwise, have a good model
        self.jitter_cnt += 1
        idx = self.jitter_cnt % len(self.jitter_seq)
        return self.findzero() + jitter * self.jitter_seq[idx]

    def converged(self):
        count_within_window = np.count_nonzero(np.abs(self.level_readings) < self.level_precision_window)
        return count_within_window >= 3 and self.residual_error < self.tolerance
