#!/usr/bin/env python3
import joint_utils
import random

# For a simple test, assume a perfect joint / level with some nonzero offset
real_zero = 8.0
# joint angle at zero is -1 * intercept

jf = joint_utils.JointLevelFinder(max_level_angle=5, level_precision_window=0.25, residual_tolerance=0.02, jitter=0.1)

noise_mag = 0.02
# Do a rough guess as if the level is saturated
for l_guess in range(-5, 15):
    j0 = 0.
    j = jf.update(j0, l_guess)
    for n in range(10):
        j_test = jf.next_testpoint()
        print(f'{n}: Next test point is {j_test}')
        noise = (random.random() - 0.5) * noise_mag
        j = jf.update(j_test, j_test - real_zero + noise)
    else:
        print(f"Converged to {j} with initial guess {l_guess} and uniform random noise magnitude {noise_mag}")

