import joint_utils
import numpy as np

joint_angles = [-3., 2., -1., 0., 1, 2,]

print(f"Joint angles: {joint_angles}")
for n in range(10):
    # Assume uniformly distributed noise around the ideal level measurement
    samples = (np.random.rand(len(joint_angles)) - 0.5) * 0.2 + joint_angles
    j_test = joint_utils.JointLevelInterpolator(joint_angles, samples)
    print(f"Level Samples: {samples}, zero angle: {j_test.findzero()}")
