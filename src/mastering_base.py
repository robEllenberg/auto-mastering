from robot_command.rpl import *
set_units("mm", "deg")

import numpy as np
import rospy
import math
import time
import datetime
from src import param_ns


class MasteringBase:
    instrument_options = {"level_front",
                          "level_side",
                          "probe_axial",
                          "123_block"
                          }

    def __init__(self, probe_dio, probe_active_low=False):
        self.offset_vals = np.array([0., 0., 0., 0., 0., 0.])
        self.mastering_log = []
        self.last_probe_pos = None
        self.probe_dio_id = probe_dio
        self.probe_active_low = probe_active_low
        self.level_settling_time = 11.
        self.current_instrument = None

        # These can be overridden by the maxvel slider
        self.default_vel = 0.5
        self.probe_traverse_vel = 0.2

    def get_log(self):
        return '\n'.join(self.mastering_log)

    def set_param(self, name, value):
        self.log(f'Setting parameter {name} = {value}')
        rospy.set_param(name, value)

    def write_mastering_data(self, filename):
        with open(filename, 'a') as f:
            f.write(f"\n Probe Pose Setup: {datetime.datetime.now()}\n")
            f.write('\n'.join(self.mastering_log))

    def notify_log(self, msg_str, **kwargs):
        """
        :param msg_str: The message to notify and log
        :param kwargs:
        :return:
        """
        rospy.loginfo(msg_str)
        self.mastering_log.append(msg_str)
        notify(msg_str, **kwargs)

    def raise_error(self, msg_str):
        """
        :param msg_str: The message to notify and log
        :return:
        """
        rospy.loginfo(msg_str)
        self.mastering_log.append(msg_str)
        notify(msg_str, error=True)

    def log(self, msg_str):
        rospy.loginfo(msg_str)
        self.mastering_log.append(msg_str)

    def input(self, msg_str):
        self.log("input: " + msg_str)
        r = input(msg_str)
        self.log("response: {}".format(r))
        return r

    def go_vertical_home(self, **kwargs):
        self.precision_move([0., 0., -90., 0., 0., 0.], **kwargs)

    @staticmethod
    def get_joint_val_array():
        return np.array(get_joint_values().to_list())

    def mastering_movej(self, master_pos_raw, base_offset=None, v=None, probe=0):
        zero_offset = base_offset if base_offset is not None else self.offset_vals
        self.log("Moving to joint pose {} with zero offset {}".format(master_pos_raw, zero_offset))
        master_pos = np.array(zero_offset) + np.array(master_pos_raw)
        retries = 0
        v = v or self.default_vel
        while retries < 3:
            try:
                retries += 1
                return movej(Joints.from_list(master_pos), v=v, probe=probe)
            except MoveError:
                # TODO better error message when we can pass all the error data back up from controller
                self.log("Probe and/or motion error, trying again")
        self.notify_log(f"Failed to reach mastering goal position. Press OK to continue anyway.",
                        warning=True)

    def precision_move(self, goal_pos, v=None, tolerance=0.01):
        """
        Work around accuracy limitations at high speed in current motion planning pipeline.
        This ensures that the final motion is long enough to be properly planned, and slow enough to not be clipped by the goal tolerance.

        Not usable for probing.
        """
        self.mastering_movej(goal_pos, v=v)
        retries = 0
        goal_with_offset = goal_pos + self.offset_vals
        worst_joint_error = 0
        while retries < 10:
            worst_joint_error = np.max(np.abs(self.get_joint_val_array() - goal_with_offset))
            if worst_joint_error <= tolerance:
                break
            rospy.loginfo(f"Worst case joint error is {worst_joint_error}, retrying")
            movef(Joints.from_list(goal_with_offset))
            # Make sure the final position is really settled
            sleep(0.1)
            retries += 1
        else:
            self.notify_log(f"Failed to reach mastering goal position within tolerance {tolerance}, max joint error is {worst_joint_error}. Press OK to continue anyway.",
                            warning=True)

    def movej_incr(self, pos_delta, v=None, **kwargs):
        v = v or self.default_vel
        goal_abs_pos = self.get_local_joint_pose() + np.array(pos_delta)
        self.log("Moving incrementally by joint angle difference {} to {}".format(pos_delta, goal_abs_pos))
        return self.precision_move(goal_abs_pos, v=v, **kwargs)

    def input_float_value_with_retry(self, msg_str, allow_blank=False):
        level_val = None
        self.log(f"Current joint angles: {self.get_local_joint_pose()}")
        while level_val is None:
            level_val_in = self.input(msg_str)
            if level_val_in == 'max':
                # TODO store this in a config somewhere
                max_val = 5.
                self.log("Level saturated in positive direction at {}".format(max_val))
                return max_val
            elif level_val_in == 'min':
                # TODO store this in a config somewhere
                min_val = -5.
                self.log("Level saturated in positive direction at {}".format(min_val))
                return min_val
            try:
                if allow_blank and not level_val_in:
                    return None
                level_val = float(level_val_in)
            except ValueError:
                self.notify_log("Value {} is not a valid number, please re-enter".format(level_val_in), warning=True)
        return level_val

    def get_user_maxvel(self):
        vmax_raw = self.input_float_value_with_retry("Enter maximum velocity for traverse moves (in %, max is 100)")
        self.default_vel = min(max(vmax_raw/100., 0.05), 1.)
        vmax_raw = self.input_float_value_with_retry("Enter maximum velocity when probe is attached (in %, max is 30)")
        # Don't allow more than 50% max speed
        self.probe_traverse_vel = min(max(vmax_raw/100., 0.05), 0.3)

    def load_existing_offsets(self):
        offsets = []
        for j in (f'joint_{n}' for n in range(6)):
            pname = f'{param_ns}/{j}_offset'
            try:
                offset_str = rospy.get_param(pname) if rospy.has_param(pname) else '0.0'
                offsets.append(float(offset_str))
            except ValueError:
                offsets.append(0.0)
        if np.max(np.abs(offsets)) > 0.0:
            is_ok = self.input('Found existing mastering offsets. Run mastering with these offsets (or clear them)? [y/N]')
            if is_ok in ('y', 'Y'):
                self.offset_vals = np.array(offsets)
        # Re-write parameters (with any missing data filled in)
        self.save_offsets()

    def save_offsets(self):
        for n in range(6):
            rospy.set_param(f'{param_ns}/joint_{n}_offset', float(self.offset_vals[n]))

    def get_param(self, name, default_val=None):
        fq_name = f'{param_ns}/{name}'
        # NOTE: need to do whatever conversion from string values in caller
        return rospy.get_param(fq_name) if rospy.has_param(fq_name) else default_val

    @staticmethod
    def countdown_notify(wait_time=0.0, msg=None):
        if wait_time > 0:
            msg = msg if msg else f"Wait for level reading to settle for {int(wait_time):d} seconds"
            notify(msg, warning=True, timeout=wait_time)

    def get_local_joint_pose(self):
        joint_angle_list = np.array(get_joint_values().to_list())
        return joint_angle_list - self.offset_vals

    def get_local_joint_angle(self, joint_number):
        joint_idx = joint_number - 1
        joint_angle_list = get_joint_values().to_list()
        j_angle = joint_angle_list[joint_idx] - self.offset_vals[joint_idx]
        return j_angle

    def zero_joint_at_current(self, joint_number, nominal_pose, temporary=False):
        joint_idx = joint_number - 1
        offset_delta = (get_joint_values().to_list()[joint_idx] - nominal_pose[joint_idx])
        old_offset = self.offset_vals[joint_idx]
        self.offset_vals[joint_idx] = offset_delta
        if not temporary:
            self.save_offsets()
        self.log(f"Zeroing joint {joint_number}, old offset {old_offset},  new offset {self.offset_vals[joint_idx]}")

    def add_offset_to_joint(self, joint_number, offset_delta_deg, temporary=False):
        if joint_number < 1 or joint_number > 6:
            self.raise_error(f"Invalid joint number {joint_number}")
        idx = joint_number - 1
        self.offset_vals[idx] += offset_delta_deg
        self.save_offsets()
        if not temporary:
            self.save_offsets()
        self.log(f"Updated joint offsets{self.offset_vals}")

    def probe_tripped(self):
        if self.probe_dio_id is None:
            raise ValueError("Probe DIO pin is not specified, cannot detect a probe trip!")
        return get_digital_in(self.probe_dio_id) ^ self.probe_active_low

    def jointspace_probe_cycle(self, probe_direction_j, rough_speed, finish_speed, total_travel_deg, reverse_dist_deg=2.):
        """
        Rough / fine probing cycle.
        :param probe_direction_j: unit vector representing a direction in joint-space to probe
        :param rough_speed: probing speed (0-1.0) for rough pass
        :param finish_speed: probing speed (0-1.0) for finish pass
        :param total_travel_deg: Crude measure of total distance in deg to move in the given jointspace direction.
        :param reverse_dist_deg: How far to back off the surface after a rough probe trip
        :return:
        """

        self.log("Starting probe in direction {}, rough speed {}, finish speed {}, total travel {}".format(
            probe_direction_j,
            rough_speed,
            finish_speed,
            total_travel_deg
        ))
        initial_pos = self.get_local_joint_pose()
        probe_direction_arr = np.array(probe_direction_j)
        probe_goal = probe_direction_arr * total_travel_deg + initial_pos
        ptype, ptime, rough_probe_jpos, rough_probe_pose = self.mastering_movej(probe_goal, v=rough_speed, probe=3)
        if not ptype:
            self.log("Failed to reach surface during rough probe")
            return False
        self.log(f"Found rough probe at joint angles {rough_probe_jpos.from_ros_units('deg').to_list()}")

        # Getting here means we got a rough trip, so back off
        retract_pose = self.get_local_joint_pose() + (probe_direction_arr * -1. * max(reverse_dist_deg, 0.1))
        self.mastering_movej(retract_pose, v=rough_speed, probe=6)

        ptype, ptime, fine_probe_jpos, fine_probe_pose = self.mastering_movej(probe_goal, v=finish_speed, probe=3)
        if not ptype:
            self.log("Failed to reach surface during finish probe")
            return False

        # Store the last probe position
        self.last_probe_pos = np.array(fine_probe_jpos.from_ros_units('deg').to_list())
        self.log(f"Found fine probe at joint angles {self.last_probe_pos}")

        # Retract from the surface back to the initial position
        self.mastering_movej(retract_pose, v=rough_speed, probe=6)
        self.mastering_movej(initial_pos, v=self.probe_traverse_vel)
        return True

    def prompt_switch_instrument(self, instrument):
        if instrument not in self.instrument_options:
            self.raise_error(f"Unknown instrument {instrument}")
        if self.current_instrument == instrument:
            return True

        # TODO track this globally as tools?
        if "level_front" in instrument:
            self.notify_log("Attach level to adapter block's front face, parallel with end-effector flange (see documentation for details)", warning=True)
        elif "level_side" in instrument:
            self.notify_log("Attach level to adapter block's side face (see documentation for details)",
                            warning=True)
        elif "probe" in instrument:
            self.notify_log("Attach touch probe to adapter block (with tip oriented along J6 axis)", warning=True)
        elif "123_block" in instrument:
            self.notify_log("Attach 123 block to end-effector mount (see documentation)", warning=True)
        self.current_instrument = instrument

    def check_joint_scale(self, test_joint_num, pose1, pose2, level_axis, intermediate_pose=None):
        """
        Compare two joint poses where the reference surface is expected to be level. Ideally, flipping the joint 180 deg
        and placing the level on the opposite surface of the parallel / 1-2-3 block should give an identical level reading
        For example, with the arm vertical, J5 @ +/-90 deg and the level on a perpendicular surface.
        """
        self.prompt_switch_instrument("123_block")
        if intermediate_pose:
            self.mastering_movej(intermediate_pose)
        self.mastering_movej(pose1)
        self.notify_log('Place level on top-facing reference surface, with level +Y towards flange. Wait for reading to settle', warning=True)
        reading1 = self.input_float_value_with_retry(f"Enter Level {level_axis} value")
        actual_pose1 = self.get_local_joint_angle(test_joint_num)
        self.notify_log('Remove level, then press ok to move to next pose', warning=True)
        if intermediate_pose:
            self.mastering_movej(intermediate_pose)
        self.mastering_movej(pose2)
        self.notify_log('Place level on top-facing reference surface, with level +Y towards flange. Wait for reading to settle', warning=True)
        reading2 = self.input_float_value_with_retry(f"Enter Level {level_axis} value")
        actual_pose2 = self.get_local_joint_angle(test_joint_num)
        self.notify_log('Remove level, then press ok to move to next pose', warning=True)

        # Assumes that only one joint is changing to avoid introducing errors
        angle_diff = abs(actual_pose2 - actual_pose1)
        # Poses are structured
        abs_angle_error = reading2 - reading1
        relative_error = (angle_diff + abs_angle_error) / angle_diff
        self.notify_log(f"Joint {test_joint_num:d}: Found absolute error {abs_angle_error:0.3f} deg, relative scale error {relative_error:0.5f}", warning=True)
        rospy.set_param(f'{param_ns}/joint_{test_joint_num}_relative_scale_error', float(relative_error))
        rospy.set_param(f'{param_ns}/joint_{test_joint_num}_abs_error_180deg', float(abs_angle_error))
        if intermediate_pose:
            self.mastering_movej(intermediate_pose)
