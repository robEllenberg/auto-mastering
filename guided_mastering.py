from src import joint_utils
from robot_command.rpl import *
set_units("mm", "deg")

import numpy as np
import rospy
import math
from setup_j2_j3_probe_position import ProbeSetup


class ManualMasteringApp(ProbeSetup):

    def __init__(self, probe_dio, probe_active_low=False):
        ProbeSetup.__init__(self, probe_dio, probe_active_low)
        self.level_settling_time = 11.

        # For tilt measurement
        self.measurements = {
            'lower_arm_ytilt_top':None,
            'j3_ytilt_delta':None,
            'j5_local_ytilt_top': None,
            'j3_twist_xy': None,
            'j3_twist_yz': None,
        }

    def jog_to_valid_level_reading(self, jog_direction, tolerance, level_axis=None):
        jog_input = 'not_ok'
        level_axis = level_axis or 'X'
        while jog_input and jog_input != "ok":
            jog_input = self.input('Enter joint angle offset to jog. Level {} should be within +/- {} degrees. Leave entry blank to accept, or abort to cancel'.format(level_axis, tolerance))
            try:
                jog_val = float(jog_input)
                self.precision_move(np.array(jog_direction) * jog_val + self.get_local_joint_pose())
            except ValueError:
                rospy.loginfo("Got {} for input that's not a valid float in jog, trying again".format(jog_input))
                continue
        return True

    def find_joint_level_zero(self, nominal_pose, search_direction, backoff_angle, joint_number, slope_guess=1.0, level_axis=None, rough_only=False):
        level_axis = level_axis or "X"
        if joint_number < 1 or joint_number > 6:
            self.raise_error("Invalid joint number {} to master, must be between 1 and 6".format(joint_number))
        joint_idx = joint_number - 1
        jmodel = joint_utils.JointLevelFinder(max_level_angle=4, level_precision_window=0.5,
                                              residual_tolerance=0.02, jitter=0.15, slope_guess=slope_guess)
        # TODO approach from both sides and compensate for any hysteresis
        search_joint_dir = np.array(search_direction)
        target_pose = np.array(nominal_pose)

        self.precision_move(target_pose)
        self.jog_to_valid_level_reading(search_direction, tolerance=1.0, level_axis=level_axis)

        self.zero_joint_at_current(joint_number, nominal_pose, temporary=True)
        if rough_only:
            return True

        for n in range(6):
            # Move away (to avoid planning errors and to approach from a consistent direction
            self.precision_move(target_pose)
            self.countdown_notify(self.level_settling_time)
            level_val = self.input_float_value_with_retry(
                "Measurement {}: Enter the current level angle in {} axis (degrees)".format(n, level_axis))
            j_angle = self.get_local_joint_pose()[n]
            jmodel.update(j_angle, level_val)
            test_pt = jmodel.next_testpoint()
            self.log("Next test angle is {}".format(test_pt))
            target_pose = np.choose(
                abs(search_joint_dir) > 0.00001,
                [nominal_pose, search_joint_dir * test_pt]
            )
            self.log(
                "j{} model: zero {}, slope {}, intercept {}, residual {}, joint angles {}, level readings {}".format(
                    joint_number,
                    jmodel.findzero(),
                    jmodel.slope,
                    jmodel.intercept,
                    jmodel.residual_error,
                    jmodel.joint_angles,
                    jmodel.level_readings,
                ))
            if jmodel.converged():
                self.log("Joint {} level model converged".format(joint_number))
                break
        else:
            self.raise_error("Failed to converge on a zero position for joint {}".format(joint_number))

        j_local_zero = jmodel.findzero() - nominal_pose[joint_idx]
        rospy.loginfo("Joint {} model fit slope is {}, intercept is {}, zero position {}".format(joint_number, jmodel.slope, jmodel.intercept, j_local_zero))
        self.add_offset_to_joint(joint_number, j_local_zero)
        self.precision_move(nominal_pose)  # Go to the zero to verify the level reading
        self.countdown_notify(self.level_settling_time)
        is_ok = self.input('Is level reading within 0.02 deg of zero? [Y/n]')
        if is_ok in ('n', 'N'):
            self.notify_log('Joint {} mastering was unsuccessful. Press OK to try again, or abort to stop', warning=True)
            # Indicate it was not successful
            return False
        return True

    def master_j6_at_home(self, j1_angle=0, rough_only=False):
        while not self.find_joint_level_zero(
            nominal_pose=[j1_angle, 0., 0., 0., 0., 0.],
            search_direction=[0., 0., 0., 0., 0., 1.],
            backoff_angle=-5.,
            joint_number=6,
            rough_only=rough_only,
        ):
            self.log("Retry mastering of J6 at home position")

    def find_angle_delta_for_pose_pair(self, first_test_pos, second_test_pos, level_axes):

        self.precision_move(first_test_pos)

        self.countdown_notify(self.level_settling_time)

        level_first = []
        for level_axis in level_axes:
            level_first.append(self.input_float_value_with_retry(f"Robot is now at first test position. Enter the current level {level_axis} reading"))
        self.precision_move(second_test_pos)
        self.countdown_notify(self.level_settling_time)
        level_second = []
        for level_axis in level_axes:
            level_second.append(self.input_float_value_with_retry(f"Robot is now at second test position: Enter the current level {level_axis} reading"))

        level_angle_delta = (np.array(level_second)-np.array(level_first))/2.
        level_avg_angle = (np.array(level_second)+np.array(level_first))/2.
        return level_angle_delta, level_avg_angle

    def master_j5_delta_method(self, angle_tol, elbow_down, max_iterations=5):
        """
        Minimize the delta between two level readings so that J4 and J6 are as
        parallel as possible, which will be the master position of J5.
        """
        elbow_pitch = -180 if elbow_down else -90
        shoulder_pitch = 90 if elbow_down else 0
        nominal_pos = [0, shoulder_pitch, elbow_pitch, 0, 0., 0]
        # Assume previously rough-mastered
        self.mastering_movej(nominal_pos)
        self.jog_to_valid_level_reading([0., 0., 1., 0., 0., 0.], tolerance=0.5, level_axis='Y')
        elbow_pitch = self.get_local_joint_angle(3)
        self.zero_joint_at_current(5, nominal_pos, temporary=True)

        # Define "negative" / "positive" test positions
        # This is with the lower arm vertical, spinning J4 and J6
        # in opposite directions to reach the second test position
        first_j5_testpos = np.array([0, shoulder_pitch, elbow_pitch, -90, 0, 90])
        second_j5_testpos = np.array([0, shoulder_pitch, elbow_pitch, 90, 0, -90])

        avg_angle = None
        dp = [0, 0]
        # Flip J5 / J6 by 180 deg to eliminate any perpendicularity error in the level adapter bracket
        # The goal is to find two J5 positions 180 deg apart such that the level reads the same value in Y
        for n in range(max_iterations):
            dp, avg_angle = self.find_angle_delta_for_pose_pair(first_j5_testpos, second_j5_testpos, "XY")
            # Level X axis is oriented in J5 plane
            j5_delta = dp[0]
            rospy.loginfo("J5 level measurement delta at +/-90 deg is {}".format(j5_delta))
            if abs(j5_delta) < angle_tol:
                break
            else:
                self.notify_log("Adjusting J5 Offset: add {:0.4f} deg".format(j5_delta))
                self.add_offset_to_joint(5, j5_delta)
        else:
            self.raise_error("Failed to orient J5 to within {} deg".format(angle_tol))

        self.notify_log(f"Net Y tilt of J5 at vertical arm pose is {dp[1]:0.4f}", warning=True)
        return avg_angle[0]

    def master_j4_delta_method(self, angle_tol, max_iterations=5):
        """
        Minimize the delta between two level readings so that J4 and J6 are as
        parallel as possible, which will be the master position of J5.
        """
        nominal_pos = [0., 0., 0., 0., -90., 0.]
        # Assume previously rough-mastered
        self.mastering_movej(nominal_pos)
        self.jog_to_valid_level_reading([0., 0., 0., 1., 0., 0.], tolerance=0.5, level_axis='X')
        self.jog_to_valid_level_reading([0., 0., 0., 0., 1., 0.], tolerance=0.5, level_axis='Y')
        self.zero_joint_at_current(4, nominal_pos, temporary=True)

        # Define "negative" / "positive" test positions
        # With J6 vertical, compare two positions when J6 is rotated to determine
        # when plane formed by J6 and J4 axes is vertical
        # Start from the current adjusted position (don't save the rough offsets to J5)
        first_j4_testpos = self.get_local_joint_pose()
        second_j4_testpos = np.array(first_j4_testpos)
        # Spin J6 by 180 deg
        second_j4_testpos[5] += 180.

        avg_angle = None
        dp = [0, 0]
        # Adjust J4 offset until J6 axis is true vertical (level reading in "X" axis does not change)
        for n in range(max_iterations):
            dp, avg_angle = self.find_angle_delta_for_pose_pair(first_j4_testpos, second_j4_testpos, "X")
            j4_delta = dp[0]
            rospy.loginfo("J4: level measurement delta is {}".format(j4_delta))
            if abs(j4_delta) < angle_tol:
                break
            else:
                self.notify_log("Adjusting J4 Offset: add {:0.4f} deg".format(j4_delta))
                self.add_offset_to_joint(4, j4_delta)
        else:
            self.raise_error("Failed to orient J4 to within {} deg".format(angle_tol))

        return avg_angle[0]

    def orient_lower_arm_vertical(self, angle_tol, max_iterations=10):
        """
        Preparation for J4/J5/J6 mastering by orienting the lower arm (forearm) vertically
        This uses the technique of checking level readings at two orientations of J4, 180 deg. apart.
        If the level reads the same in both orientations, then the J4 axis is pointed vertically
        (as projected on the XZ plane). Note that if J3 is not parallel with the ground,
        then J4's axis will be tilted in the YZ plane still.

        Level must be mounted in the first position (base perpendicular to J6)
        """
        nominal_pos = [0, 0, -90, -90, 0, 0]
        # Assume previously rough-mastered
        self.mastering_movej(nominal_pos)
        self.jog_to_valid_level_reading([0., 0., -1., 0., 0., 0.], tolerance=0.5, level_axis='X')
        self.zero_joint_at_current(3, nominal_pos, temporary=True)
        self.jog_to_valid_level_reading([0., 0., 0., 0., 1., 0.], tolerance=0.5, level_axis='Y')
        self.zero_joint_at_current(5, nominal_pos, temporary=True)
        # Move away from the zero point to start

        # Define "negative" / "positive" test positions for lower arm to orient J4 vertically
        first_lowerarm_testpos = np.array([0, 0, -90, -90, 0, 0])
        second_lowerarm_testpos = np.array(first_lowerarm_testpos)
        second_lowerarm_testpos[3] += 180

        dp = [0,0]
        # Move J4 to +/-90 to compare level readings. When they are identical, the lower arm is vertical (in the XZ plane)
        # The resulting difference is used to adjust the J3 offset, so that J4's axis is as close to vertical as possible
        for n in range(max_iterations):
            dp, avg_angle = self.find_angle_delta_for_pose_pair(first_lowerarm_testpos, second_lowerarm_testpos, "XY")
            # Flip sign due to J3 orientation relative to the test pose pair
            j3_delta = -dp[0]
            j5_rough_delta = -avg_angle[1]
            rospy.loginfo("J4 level measurement delta at +/-90 deg is {}".format(j3_delta))
            if abs(j3_delta) < angle_tol and abs(j5_rough_delta) < angle_tol:
                self.notify_log(f"Lower arm link is now vertical: (level reading delta is {j3_delta:0.4f})", warning=True)
                break
            else:
                self.notify_log("Adjusting J3 offset: add {:0.4f} deg".format(j3_delta))
                self.add_offset_to_joint(3, j3_delta)
                self.notify_log("Adjusting J5 rough offset: add {:0.4f} deg".format(j5_rough_delta))
                self.add_offset_to_joint(5, j5_rough_delta)
        else:
            self.raise_error("Failed to orient lower arm link vertically to within {:0.4f} deg".format(angle_tol))

        self.measurements['lower_arm_ytilt_top'] = dp[1]
        self.notify_log(f"At vertical arm pose, upper arm Y tilt is {self.measurements['lower_arm_ytilt_top']:0.4f} deg")
        # Now that arm is oriented to true vertical in XZ plane

    def level_base(self, angle_tolerance, max_iterations=10):
        base_measure_pos = np.array([-90, 0, 0, 0, -90, 0])

        self.mastering_movej(base_measure_pos)
        self.jog_to_valid_level_reading([0,0,0,1,0,0], tolerance=0.5, level_axis='X')
        self.zero_joint_at_current(4, base_measure_pos, temporary=True)
        self.jog_to_valid_level_reading([0,0,0,0,1,0], tolerance=0.5, level_axis='Y')
        self.zero_joint_at_current(5, base_measure_pos, temporary=True)

        for n in range(max_iterations):
            # In the first position, the level readings line up with the robot's base coordinate axes.
            # A positive level reading means a tilt "up" in that axis (i.e., the X+ / Y+ side of the base is higher)
            # In the second position, the sense is reversed.

            dp, avg_angle = self.find_angle_delta_for_pose_pair(base_measure_pos, base_measure_pos + [180., 0., 0., 0., 0., 0.], "XY")

            tilt_xplus_up = dp[0]
            tilt_yplus_up = dp[1]

            # Move J5 / J6 so the level is perpendicular to J1's axis
            # Using average angle here to place the level perpendicular to J1 at the first test position,
            # so users can tweak the base until the level reads 0 in both axes
            # Works because axis tilt phi = (theta2-theta1)/2, and level is perpendicular to J1 when corrected
            # by theta2-phi = theta2-(theta2-theta2)/2 = (theta2 + theta1)/2
            net_angle_j5 = -avg_angle[1]
            net_angle_j4 = -avg_angle[0]
            self.add_offset_to_joint(5, net_angle_j5)
            self.add_offset_to_joint(4, net_angle_j4)

            # Return to the original position so user can directly read the level to adjust the base
            self.precision_move(base_measure_pos)

            if abs(tilt_xplus_up) > angle_tolerance or abs(tilt_yplus_up) > angle_tolerance:
                self.notify_log(
                    f"Base is tilted {tilt_xplus_up:0.4f} deg in X and {tilt_yplus_up:0.4f} deg in Y. Adjust base until level reads zero in both axes.",
                    warning=True)
            else:
                return True
        else:
            self.notify_log("Robot base is not levelled! Press Ok to continue anyway, or Abort to stop", warning=True)
            return False

    def probe_j2_j3(self, direction, probe_distance_mm=60., probe_rough_vel=0.02, probe_finish_vel=0.005):

        pitch_offset = float(self.get_param('j2_j3_probe_pitch_offset', 12.0))
        yaw_offset = float(self.get_param('j2_j3_probe_yaw_offset', 0))
        p_setup_sequence = self.plan_probe_prep_sequence(pitch_offset, yaw_offset, direction)

        for ps in p_setup_sequence[0:-1]:
            # Cut corners and don't check probe during the main traverse
            self.mastering_movej(ps, v=self.probe_traverse_vel)
        # Slow this down for the final step even if user wants it faster
        self.mastering_movej(p_setup_sequence[-1], v=0.1)

        sts = ""
        while True:
            self.notify_log(f"{sts}Press OK to begin probing move, maxdepth {probe_distance_mm} mm", warning=True)
            probing_direction = np.array([0, direction, -direction, 0, 0, 0])
            # Start probing motion
            dpr = 180. / math.pi

            # Approximate based on initial position
            deg_per_mm = 1. / math.cos(pitch_offset / dpr) / 454. * dpr
            total_distance_deg = deg_per_mm * probe_distance_mm
            if not self.jointspace_probe_cycle(probing_direction, probe_rough_vel, probe_finish_vel, total_distance_deg):
                sts = "No probe signal detected: "
                self.mastering_movej(p_setup_sequence[-1], v=0.05)
            else:
                break

        for ps in reversed(p_setup_sequence):
            # Unwind the setup sequence to get back to close to the master position
            self.mastering_movej(ps, v=self.probe_traverse_vel)

        return self.last_probe_pos[1]

    def master_j2_j3_with_probe(self, tolerance):
        probe_dist_mm = 80.
        for n in range(5):
            # self.notify_log("Press OK to move to first probe position", warning=True)
            j2_probe_pos = self.probe_j2_j3(
                direction=1.,
                probe_distance_mm=probe_dist_mm,
            )
            # self.notify_log("Press OK to move to second probe position", warning=True)
            j2_probe_neg = self.probe_j2_j3(
                direction=-1.,
                probe_distance_mm=probe_dist_mm,
            )
            j2_delta = (j2_probe_pos + j2_probe_neg) / 2.0 - self.offset_vals[1]
            self.notify_log("Got j2 angle {:0.5f} (pos orientation) and {:0.5f} (neg orientation), adjusting offset of j2 / j3 by {:0.5f}".format(
                j2_probe_pos - self.offset_vals[1],
                j2_probe_neg - self.offset_vals[2],
                j2_delta
            ), warning=True)

            # J2 and J3 need to have opposite offsets applied to maintain the already mastered orientation of j4 and above
            self.add_offset_to_joint(2, j2_delta)
            self.add_offset_to_joint(3, -j2_delta)
            if abs(j2_delta) < tolerance:
                self.log("J2 / J3 mastering complete, ")
                return
        else:
            self.raise_error("Failed to converge on a J2 / J3 solution")

    def check_j3_ytilt(self):
        self.mastering_movej([0, 0, 0, 0, 0, 0])
        self.mastering_movej([0, 90, -180, 0, 90, 0])
        self.countdown_notify(self.level_settling_time)
        x1 = self.input_float_value_with_retry("Robot has reached first position. Enter X level reading")
        self.mastering_movej([0, 0, -90, 0, 90, 0])
        self.countdown_notify(self.level_settling_time)
        x2 = self.input_float_value_with_retry("Robot has reached second position. Enter X level reading")
        self.mastering_movej([0, 0, 0, 0, 0, 0])
        self.measurements['j3_ytilt_delta'] = x2 - x1
        self.notify_log(f"J3 Y tilt difference between horizontal and vertical is {self.measurements['j3_ytilt_delta']}", warning=True)

    def check_j3_twist(self):
        """
        Measure "twist" between J2 and J3 axes (i.e. difference in angle as measured in J4 axis direction when arm is vertical)
        """
        self.go_vertical_home()

        poses = [
            [0, -90, 0, 0, 0, 0],
            [0, -90, 0, 0, 0, 180],
            [0, 0, -90, 0, 0, 0],
            [0, 0, -90, 0, 0, 180],
            [0, 90, -180, 0, 0, 0],
            [0, 90, -180, 0, 0, 180],
        ]
        dp_1, x1 = self.find_angle_delta_for_pose_pair(poses[0], poses[1], 'X')
        dp_2, x2 = self.find_angle_delta_for_pose_pair(poses[2], poses[3], 'X')
        dp_3, x3 = self.find_angle_delta_for_pose_pair(poses[4], poses[5], 'X')

        self.log(f"J3 twist raw_data: {dp_1} {x1} {dp_2} {x2} {dp_3} {x3}")
        self.go_vertical_home()
        self.measurements['j3_twist_yz'] = dp_2[0] - (dp_3[0] + dp_1[0])/2.
        self.measurements['j3_twist_xy'] = (dp_3[0] - dp_1[0])/2.
        self.notify_log(f"J3 twist is {self.measurements['j3_twist_xy']:0.4f} deg in XY plane and {self.measurements['j3_twist_yz']:0.4f} deg in YZ plane", warning=True)

    def check_j5_ytilt(self):
        # NOTE: this is the total Y tilt summed up for all joints up to J5. The measurment can't by itself isolate how tilted J5 is relative to the lower arm.
        self.mastering_movej([0, 0, 0, 0, 0, 0])
        self.mastering_movej([0, 0, -90, 0, 90, 0])
        self.countdown_notify(self.level_settling_time)
        x1 = self.input_float_value_with_retry("Robot has reached first position. Enter X level reading")
        self.mastering_movej([0, 0, -90, 0, 90-180, -180])
        self.countdown_notify(self.level_settling_time)
        x2 = self.input_float_value_with_retry("Robot has reached second position. Enter X level reading")
        self.mastering_movej([0, 0, 0, 0, 0, 0])
        self.measurements['j5_stacked_ytilt_top'] = (x2 - x1)/2.
        self.notify_log(f"J5 overall Y tilt (at vertical arm pose) {self.measurements['j5_stacked_ytilt_top']}", warning=True)
        if self.measurements['lower_arm_ytilt_top'] is not None:
            self.measurements['j5_local_ytilt_top'] = self.measurements['j5_stacked_ytilt_top'] - self.measurements['lower_arm_ytilt_top']
            self.notify_log(f"J5 Y local tilt in Y (at vertical arm pose) is {self.measurements['j5_local_ytilt_top']}", warning=True)

    # Note: the poses below are chosen so that a positive difference in the level readings
    # means the scale is too big, and a negative difference means it's too small
    def check_j2_scale(self):
        """
        Measure J2 scaling using a straight-arm pose. The arm is lowered to the horizontal position, and
        J1 rotation keeps the arm on the same side of the robot base to avoid flexing the stand.
        NOTE: this assumes that J1 / robot base have been leveled! If it has not been than the results will be off by 2x the J1 tilt.
        """
        pose1 = [-90, -90, -90, 0, 0, 0]
        pose2 = [90, 90, -90, 0, 0, 0]
        intermediate_pose = [0, 0, -90, 0, 0, 0]
        self.check_joint_scale(2, pose1, pose2, 'Y', intermediate_pose)

    def check_j3_scale(self, pitch_offset=0):
        po = min(max(pitch_offset, -30), 30)
        j2 = 60 + po
        j3 = -j2-90
        pose2 = [0, j2, j3+180, 0, -90, 0]
        pose1 = [0, j2, j3, 0, -90, 0]
        self.check_joint_scale(3, pose1, pose2, 'Y')

    def check_j4_scale(self):
        pose2 = [0, 0, 0, -90, 0, 90]
        pose1 = [0, 0, 0, 90, 0, 90]
        self.check_joint_scale(4, pose1, pose2, 'X')

    def check_j5_scale(self, pitch_offset=0):
        j2 = 45 + min(max(pitch_offset, -10), 50)
        pose1 = [0, j2, -90 - j2, 0, -90, 0]
        pose2 = [0, j2, -90 - j2, 0, 90, 0]
        self.check_joint_scale(5, pose1, pose2, 'Y')

    def check_j6_scale(self):
        pose1 = [0, 0, 0, 0, 0, 0]
        pose2 = [0, 0, 0, 0, 0, 180]
        self.check_joint_scale(6, pose1, pose2, 'X')


def main():
    master = ManualMasteringApp(probe_dio=1, probe_active_low=True)

    # master.notify_log("Testing probe at top position", warning=True)
    # master.mastering_movej([0, 0, -90, 0, 90, 0])
    # if master.joint_step_probe([0,0,1,0,1,0],0.5,0.1,10):
    #     master.notify_log("Probe works, continuing with mastering?", warning=True)
    # else:
    #     master.raise_error("Probe failed to trip")
    #     exit()
    try:
        master.load_existing_offsets()
        angle_tol = 0.01
        if master.input('Check level of robot base? [Y/n]') not in ('N', 'n'):
            # NOTE: level base is a prerequisite for other mastering / QC operations
            # master.master_j6_at_top(j1_angle=-90, rough_only=True)
            # master.master_j5_at_top(j1_angle=-90, rough_only=True)
            master.prompt_switch_instrument("level_front")
            master.level_base(angle_tol)

        if master.input('Check joint scales? [y/N]') in ('Y', 'y', 'yes'):
            # Start at zero position to switch instrument
            master.mastering_movej([0, 0, 0, 0, 0, 0])
            master.prompt_switch_instrument("123_block")
            master.notify_log("WARNING: Remove any obstacles so that arm can fully extend horizontally in +X, +Y and -Y directions", warning=True)
            master.check_j6_scale()
            master.check_j4_scale()
            master.check_j5_scale()
            master.check_j3_scale()
            master.check_j2_scale()

        # Raise the arm up and rough level J5 / J6, to prepare to orient J4 vertically
        if master.input('Perform J4/J5/J6 mastering with level? [Y/n]') not in ('N', 'n'):
            master.prompt_switch_instrument("level_front")

            # Start with a rough master for J6 because it's arbitrary (no rotational constraint on the level block anyway)
            # Just needs to be good enough so that "X" and "Y" level directions are close to the expected orientation
            master.master_j6_at_home(rough_only=True)
            master.orient_lower_arm_vertical(angle_tol)
            # Re-master these now that J4 / lower arm link is actually vertical
            # Now, master J5 so that J4 and J6 axes are as close to parallel as possible
            use_forward = master.input('Measure J5 delta in "down-elbow" position? [Y/n]') not in ('N', 'n')
            master.master_j5_delta_method(angle_tol, use_forward)

            # Now that arm orientation is known and J5 is mastered, J4 can be isolated and mastered
            master.master_j4_delta_method(angle_tol)

            master.go_vertical_home()
            # J4 at home returns to home position automatically
            master.notify_log("J4, J5, and J6 are now mastered, and the Robot is now at the adjusted home position. Click Ok to continue, or Abort to stop", warning=True)

        if master.input('Check axis tilt / twist [Y/n]') not in ('N', 'n'):
        #     master.prompt_switch_instrument("level_front")
        #     master.check_j3_ytilt()
        #     master.check_j5_ytilt()
            # Others are checked during mastering
            master.check_j3_twist()

        # if master.input('Check joint scaling? [Y/n]') not in ('N', 'n'):
        #     master.prompt_switch_instrument("level_front")
        #
        #     master.check_j2_scale()
        #     master.check_j3_scale()
        #     master.check_j5_j6_scale()

        if master.input('Perform J2/J3 mastering with probe? [Y/n]') not in ('N', 'n'):
            # Start at zero position to switch instrument
            master.mastering_movej([0, 0, 0, 0, 0, 0])
            master.prompt_switch_instrument("probe_axial")
            master.master_j2_j3_with_probe(tolerance=0.01)

        master.notify_log("Mastering measurement complete, moving to vertical position to store home offset")
        master.go_vertical_home()
        master.notify_log("Go to Settings page, power off the robot, and click 'ZERO ALL JOINTS' to save home offset")
        master.log("Measured relative joint offsets:")
        for n, j in enumerate(master.offset_vals):
            master.log(f"Joint {n}: {j:0.6f} deg, {j*math.pi/180.:0.8f} rad")
    finally:
        master.write_mastering_data('robot_mastering.log')

    exit()
