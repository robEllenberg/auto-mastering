from robot_command.rpl import *
set_units("mm", "deg")

import numpy as np
import math

from src import param_ns, MasteringBase


class ProbeSetup(MasteringBase):

    def __init__(self, probe_dio, probe_active_low=False):
        MasteringBase.__init__(self, probe_dio, probe_active_low)

    def plan_probe_prep_sequence(self, pitch_offset, j1_offset, direction):
        """
        :return: a list of lists of joint angles that lead from a safe initial position to the probe clearance position
        """

        r2d = 180./math.pi
        # Hard-coded kinematics values to tune the probe poses
        d_1 = 503.
        d_2 = 380.
        j5_to_tip = 240

        theta_1 = math.atan(j5_to_tip / d_1) * r2d
        theta_2 = math.atan(j5_to_tip / d_2) * r2d

        est_tip_delta = 135.
        phi = math.asin(est_tip_delta/(2. * j5_to_tip)) * r2d

        if direction > 0:
            j1_angle = 90. + theta_1 + j1_offset
            j4_angle = -90. + (theta_1 + phi)
        else:
            j1_angle = -90. - theta_2 + j1_offset
            j4_angle = -90. - (theta_2 - phi)

        j2_pstart = 1. * direction * (90. - pitch_offset)
        j3_pstart = -90. + direction * (90. + pitch_offset)
        # Tweaked the offset by 8 deg to line up the probe tip
        j5_angle = 90.
        j6_angle = 0.

        j2_margin_deg = 5.0
        j2_approach = j2_pstart - direction * j2_margin_deg
        return (
            #[0, 0, -90, 0, 0, 0], # Vertical
            [j1_angle, 0, j3_pstart, 0, j5_angle, j6_angle],
            #[j1_angle, 0, j3_pstart, j4_angle, j5_angle, j6_angle],
            [j1_angle, j2_approach, j3_pstart, j4_angle, j5_angle, j6_angle],
            [j1_angle, j2_pstart, j3_pstart, j4_angle, j5_angle, j6_angle],
        )

    def find_probe_starting_poses(self):
        pitch_offset = float(self.get_param('j2_j3_probe_pitch_offset', 12.0))
        yaw_offset = float(self.get_param('j2_j3_probe_yaw_offset', 0))

        # Negative direction is worse, so just calibrate for this one
        direction = -1
        while True:
            # self.notify_log("Press OK to move to first probe position", warning=True)
            probe_setup_seq = self.plan_probe_prep_sequence(
                pitch_offset=pitch_offset,
                j1_offset=yaw_offset,
                direction=direction
            )
            for p_angles in probe_setup_seq:
                self.precision_move(p_angles, v=self.probe_traverse_vel)

            delta_pitch = 0.0
            while delta_pitch is not None:
                pitch_offset -= delta_pitch
                self.movej_incr(np.array([0, -direction, direction, 0, 0, 0]) * min(max(delta_pitch, -2.), 2.))
                delta_pitch = self.input_float_value_with_retry("Enter J2 / J3 pitch angle delta (positive = move in +Z)", allow_blank=True)

            delta_yaw = 0.0
            while delta_yaw is not None:
                yaw_offset -= delta_yaw
                self.movej_incr([-min(max(delta_yaw, -5.), 5.), 0, 0, 0, 0, 0])
                delta_yaw = self.input_float_value_with_retry("Enter J1 yaw angle delta (positive = move in +Y)", allow_blank=True)

            is_ok = self.input('Is the initial position acceptable? [y/N]')
            if is_ok in ('y', 'Y'):
                self.set_param('j2_j3_probe_pitch_offset', pitch_offset)
                self.set_param('j2_j3_probe_yaw_offset', yaw_offset)
                return probe_setup_seq
        return None


def main():
    # TODO handle other probe inputs
    probe_setup = ProbeSetup(probe_dio=1, probe_active_low=True)

    try:
        probe_setup.load_existing_offsets()

        # Start at zero position to switch instrument
        probe_setup.mastering_movej([0, 0, 0, 0, 0, 0])
        probe_setup.prompt_switch_instrument("probe_axial")
        probe_setup.find_probe_starting_poses()

    finally:
        probe_setup.write_mastering_data('robot_mastering.log')

    exit()
